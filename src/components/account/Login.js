import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import firebase from "../../firebase";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";

const loginImageNz = require("../../images/new-zealand-landscape.jpg");
const loginImageKitty = require("../../images/login-kitty.png");

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(" + loginImageNz + ")",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "dark"
        ? theme.palette.grey[900]
        : theme.palette.grey[50],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  loginIcon: {
    height: "80px",
    width: "80px",
  },
  alert: {
    marginBottom: "10px",
    marginTop: "10px",
  },
  link: {
    cursor: "pointer",
  },
  emailValidationLink: {
    marginBottom: "10px",
  },
}));

export default function SignInSide(props) {
  let history = useHistory();
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [showAlertResetPassword, setShowAlertResetPassword] = useState(false);
  const [showAlertUnverifiedEmail, setShowUnverifiedEmail] = useState(false);
  const [showAlertUnknownEmail, setShowAlertUnknownEmail] = useState(false);
  const [books, setBooks] = useState(0);

  useEffect(() => {
    firebase
      .firestore()
      .collection("books")
      .orderBy("createdAt", "desc")
      .get()
      .then((querySnapshot) => {
        let retrievedBooks = [];

        querySnapshot.forEach((doc) => {
          retrievedBooks.push(doc.data());
        });

        setBooks(retrievedBooks);
      });
  }, []);

  function resetPassword() {
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(function () {
        setShowAlertResetPassword(true);
        setShowAlert(false);
      })
      .catch(function (error) {
        setShowAlertUnknownEmail(true);
      });
  }

  function login() {
    setShowAlertUnknownEmail(false);

    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        if (res.user.emailVerified) {
          if (books.length < 2) {
            history.push("/home");
          } else {
            history.push("/books");
          }
        } else {
          setShowUnverifiedEmail(true);
        }
      })
      .catch(function (error) {
        setShowAlert(true);
      });
  }

  function sendEmailVerificationLink() {
    firebase
      .auth()
      .currentUser.sendEmailVerification()
      .then(function () {
        alert(
          "Un email de vérification vous a été envoyé, merci de le valider"
        );
        setShowUnverifiedEmail(false);
      })
      .catch(function (error) {
        console.log(error.message);
      });
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar
            className={classes.loginIcon}
            alt="Kitty"
            src={loginImageKitty}
          />
          <Typography component="h1" variant="h5">
            Connexion
          </Typography>
          <form
            className={classes.form}
            onSubmit={(e) => e.preventDefault() && false}
          >
            {showAlert && (
              <Alert
                className={classes.alert}
                variant="outlined"
                severity="error"
              >
                Email ou mot de passe incorrect
              </Alert>
            )}
            {showAlertResetPassword && (
              <Alert
                className={classes.alert}
                variant="outlined"
                severity="success"
              >
                Un email vous a été envoyé afin de réinitialiser votre mot de
                passe
              </Alert>
            )}
            {showAlertUnknownEmail && (
              <Alert
                className={classes.alert}
                variant="outlined"
                severity="error"
              >
                L'adresse email : {email} est inconnue
              </Alert>
            )}
            {showAlertUnverifiedEmail && (
              <div>
                <Alert
                  className={classes.alert}
                  variant="outlined"
                  severity="warning"
                >
                  Votre email n'a pas été vérifié
                </Alert>
                <div className={classes.emailValidationLink}>
                  <Link
                    className={classes.link}
                    onClick={sendEmailVerificationLink}
                    variant="body2"
                  >
                    Renvoyer le lien de vérification de mon adresse mail
                  </Link>
                </div>
              </div>
            )}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              value={email}
              autoComplete="email"
              onChange={(e) => setEmail(e.target.value)}
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Mot de passe"
              type="password"
              id="password"
              value={password}
              autoComplete="current-password"
              onChange={(e) => setPassword(e.target.value)}
            />
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => login()}
            >
              Connexion
            </Button>
            <Grid container>
              <Grid item xs>
                <Link
                  className={classes.link}
                  onClick={resetPassword}
                  variant="body2"
                >
                  Mot de passe oublié ?
                </Link>
              </Grid>
              <Grid item>
                <Link
                  className={classes.link}
                  onClick={() => history.push("/register")}
                  variant="body2"
                >
                  Pas encore de compte ?
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}
