import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import firebase from "../../firebase";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";

const loginImageNz = require("../../images/new-zealand-landscape.jpg");

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(" + loginImageNz + ")",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "dark"
        ? theme.palette.grey[900]
        : theme.palette.grey[50],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  link: {
    cursor: "pointer",
  },
}));

export default function Register(props) {
  const history = useHistory();
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [errorPassword, setErrorPassword] = useState(false);
  const passwordInformation =
    "Doit contenir au moins 8 caractères dont 1 minuscule, 1 majuscule, 1 chiffre, 1 caractère spécial parmi !@#$%^&*";

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Je crée mon compte
          </Typography>
          {showAlert && (
            <Alert
              className={classes.alert}
              variant="outlined"
              severity="error"
            >
              Email mal formaté
            </Alert>
          )}
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              autoComplete="email"
              onChange={(e) => setEmail(e.target.value)}
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Mot de passe"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(e) => setPassword(e.target.value)}
              helperText={passwordInformation}
              error={errorPassword}
            />
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={onRegister}
            >
              S'enregistrer
            </Button>
            <Grid container justify={"flex-end"}>
              <Grid item>
                <Link
                  className={classes.link}
                  onClick={() => history.push("login")}
                  variant="body2"
                >
                  Déjà un compte ?
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
    </Grid>
  );

  function onRegister() {
    /**
     *   Password must be :
     * - At least 8 characters long, max length anything
     * - Include at least 1 lowercase letter
     * - 1 capital letter
     * - 1 number
     * - 1 special character => !@#$%^&*
     */
    let isPasswordStrong = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[\w!@#$%^&*]{8,}$/.test(
      password
    );

    if (!isPasswordStrong) {
      setErrorPassword(true);
      return false;
    } else {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then((authData) => {
          console.log(authData.user.uid);
          firebase
            .auth()
            .currentUser.sendEmailVerification()
            .then(function () {
              firebase
                .firestore()
                .collection("users")
                .add({
                  email: email,
                  uid: authData.user.uid,
                  role: "visitor",
                })
                .then(function (docRef) {
                  alert(
                    "Un email de vérification vous a été envoyé, merci de le valider"
                  );
                  history.push("/login");
                })
                .catch(function (error) {
                  console.error("Error adding document: ", error);
                });
            })
            .catch(function (error) {
              console.log(error.message);
            });
        })
        .catch((error) => {
          setShowAlert(true);
        });
    }
  }
}
