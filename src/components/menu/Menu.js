import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "./Drawer";

import "../../App.css";

const useStyles = makeStyles(theme => ({
  root: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  },
  mainDiv: {
      marginBottom: "20px"
  },
  header: {
    backgroundColor: "#778e8d"
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between"
  },
  title: {
    fontFamily: "'Shadows Into Light', cursive",
    fontSize: "150%"
  }
}));

export default function Menu(props) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <div className={classes.mainDiv}>
        <CssBaseline />
        <AppBar className={classes.header}>
          <Toolbar className={classes.toolbar}>
            <Drawer />
            <Typography className={classes.title} variant="h6">
              New Zealand Trip
            </Typography>
          </Toolbar>
        </AppBar>
        <Toolbar id="back-to-top-anchor" />
      </div>
    </React.Fragment>
  );
}
