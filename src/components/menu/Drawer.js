import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuIcon from "@material-ui/icons/Menu";
import firebase from "../../firebase";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import PermMediaIcon from "@material-ui/icons/PermMedia";
import { useHistory } from "react-router-dom";
import AddBoxIcon from "@material-ui/icons/AddBox";

const useStyles = makeStyles({
  list: {
    width: 250,
    height: "100%",
  },
  fullList: {
    width: "auto",
  },
  logoutItem: {
    position: "absolute",
    bottom: "10px",
  },
});

export default function TemporaryDrawer() {
  let history = useHistory();
  const classes = useStyles();
  const [state, setState] = useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    firebase
      .firestore()
      .collection("users")
      .where("uid", "==", firebase.auth().currentUser.uid)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          setIsAdmin(doc.data().role === "admin");
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }, []);

  function handleBooks() {
    history.push("/books");
  }

  const toggleDrawer = (side, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = (side) => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List className={classes.list}>
        <ListItem button key={"books"} onClick={handleBooks}>
          <ListItemIcon>
            <PermMediaIcon />
          </ListItemIcon>
          <ListItemText primary={"Carnets"} />
        </ListItem>
        <Divider />
        {isAdmin && (
          <ListItem
            button
            key={"createArtile"}
            onClick={() => history.push("/admin/articles/new")}
          >
            <ListItemIcon>
              <AddBoxIcon />
            </ListItemIcon>
            <ListItemText primary={"Créer un article"} />
          </ListItem>
        )}
        <ListItem
          className={classes.logoutItem}
          button
          key={"Logout"}
          onClick={() => {
            firebase
              .auth()
              .signOut()
              .then(() => {
                history.push("/login");
              });
          }}
        >
          <ListItemIcon>
            <ExitToAppIcon />
          </ListItemIcon>
          <ListItemText primary={"Déconnexion"} />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <Button onClick={toggleDrawer("left", true)}>
        <MenuIcon htmlColor="#fff" />
      </Button>
      <Drawer open={state.left} onClose={toggleDrawer("left", false)}>
        {sideList("left")}
      </Drawer>
    </div>
  );
}
