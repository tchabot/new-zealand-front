import React from "react";
import { useHistory } from "react-router-dom";
import Link from "@material-ui/core/Link";

export default function Footer() {
  const history = useHistory();
  const styles = {
    grid: {
      backgroundColor: "#EADDC4",
    },
    link: {
      color: "black",
    },
    divLink: {
      backgroundColor: "#EADDC4",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      height: "55px",
      position: "absolute",
      width: "100%",
      bottom: 0,
    },
  };

  return (
    <div style={styles.divLink}>
      <div>
        <Link
          style={styles.link}
          component="button"
          variant="body2"
          onClick={() => history.push("/legal-notice")}
        >
          Mentions légales
        </Link>
      </div>
      <div>
        <small>&copy; Copyright 2020</small>
      </div>
    </div>
  );
}
