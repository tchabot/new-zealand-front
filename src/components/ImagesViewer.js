import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from "@material-ui/core/GridListTile";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        //width: 500,
        //height: 450,
    },
}));

export default function ImageGridList(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <GridList cellHeight={250} className={classes.gridList} cols={3}>
                {props.images.map((image, index) => (
                    <GridListTile key={index} cols={2}>
                        <img src={image.storageUrl} alt="" />
                    </GridListTile>
                ))}
            </GridList>
        </div>
    );
}
