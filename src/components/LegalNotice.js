import React from "react";
import Menu from "./menu/Menu";
import Footer from "./Footer";
import Grid from "@material-ui/core/Grid";
import { Container } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

export default function LegalNotice() {
  const styles = {
    container: {
      marginTop: "20px",
      maxWidth: "85%",
    },
    title: {
      fontSize: "50px",
    },
    paper: {
      padding: "20px",
    },
    gridItem: {
      marginTop: "10px",
    },
  };

  return (
    <div>
      <Menu />
      <Container>
        <Typography
          variant="h1"
          component="h2"
          gutterBottom
          style={styles.title}
        >
          Mentions Légales
        </Typography>
        <Grid container style={styles.container} justify="center">
          <Grid item xs={12} style={styles.gridItem}>
            <Paper style={styles.paper} elevation={5}>
              <b>HÉBERGEMENT</b>
              <br />
              <br />
              Le site est hébergé par Firebase, 188 King ST, San Francisco, CA
              94107, United States.
            </Paper>
          </Grid>
        </Grid>
      </Container>
      <Footer />
    </div>
  );
}
