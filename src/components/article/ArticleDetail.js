import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Menu from "../menu/Menu";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import frLocale from "date-fns/locale/fr";
import LocalizedUtils from "../lib/CustomDatePicker";
import { DropzoneArea } from "material-ui-dropzone";
import firebase from "../../firebase";
import { red } from "@material-ui/core/colors";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import LinearProgress from "@material-ui/core/LinearProgress";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import { useHistory, useParams } from "react-router-dom";
import imageCompression from "browser-image-compression";

const styles = {
  root: {
    backgroundColor: "white",
  },
  formElements: {
    padding: "2px",
    textAlign: "center",
    width: "100%",
    margin: "auto 0",
  },
  formControl: {
    margin: "1px",
    width: "100%",
  },
  formRoot: {
    padding: "5px 20px 20px 20px",
  },
  title: {
    marginBottom: "30px",
    fontSize: "25px",
  },
  buttonGrid: {
    display: "flex",
    justifyContent: "center",
  },
  deleteButton: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
    backgroundColor: red,
  },
  loader: {
    width: "70%",
    height: "100px",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: "auto",
  },
  loaderText: {
    width: "100%",
    height: "265px",
    position: "absolute",
    top: "0px",
    bottom: "0px",
    left: "0px",
    right: "0px",
    margin: "auto",
    fontWeight: "bold",
  },
  uploadText: {
    textAlign: "center",
  },
};

export default function ArticleDetail(props) {
  let history = useHistory();
  let slug = useParams();
  const inputLabel = React.useRef(null);
  const [currentArticle, setCurrentArticle] = useState({
    id: "",
    title: "",
    bookId: "",
    text: "",
    images: [],
    author: {},
    date: new Date(),
    pageTitle: "Ajouter un article",
    isArticleExists: true,
  });
  const [lastArticle, setLastArticle] = useState(null);
  const [pageTitle, setPageTitle] = useState("Ajouter un article");
  const [isArticleExists, setIsArtileExists] = useState(false);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [progress, setProgress] = useState(5);
  const [onLoading, setOnLoading] = useState(false);
  const [authors, setAuthors] = useState(["Thomas", "Pauline"]);
  const [labelWidth, setLabelWidth] = React.useState(0);
  const [isAdmin, setIsAdmin] = useState(false);
  const [books, setBooks] = useState([]);

  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);

    if (slug && slug.id && slug.id !== "new") {
      //Get the article to update
      firebase
        .firestore()
        .collection("articles")
        .where("id", "==", parseInt(slug.id))
        .get()
        .then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            let article = doc.data();

            setCurrentArticle({
              id: article.id,
              title: article.title,
              bookId: article.bookId,
              text: article.text,
              images: article.images,
              author: article.author,
              date: article.date,
            });
            setPageTitle("Modifier un article");
            setIsArtileExists(true);
          });
        })
        .catch(function (error) {
          console.log("Error getting documents: ", error);
        });
    }

    //Retrieve all books
    firebase
      .firestore()
      .collection("books")
      .orderBy("createdAt", "desc")
      .get()
      .then((querySnapshot) => {
        let retrievedBooks = [];
        querySnapshot.forEach((doc) => {
          retrievedBooks.push(doc.data());
        });
        setBooks(retrievedBooks);
      });
  }, []);

  //Get last article
  useEffect(() => {
    firebase
      .firestore()
      .collection("articles")
      .orderBy("date", "desc")
      .limit(1)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          setLastArticle(doc.data());
        });
      });
  }, []);

  //Get the user and check if is admin
  useEffect(() => {
    firebase
      .firestore()
      .collection("users")
      .where("uid", "==", firebase.auth().currentUser.uid)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          setIsAdmin(doc.data().role === "admin");
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }, []);

  //Create an article
  function addArticle(article) {
    firebase
      .firestore()
      .collection("articles")
      .add({
        id: article.id,
        title: article.title,
        bookId: article.bookId,
        text: article.text,
        images: article.images,
        author: article.author,
        date: article.date,
      })
      .then(function () {
        history.push("/home");
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
  }

  //Update an article
  function updateArticle(article) {
    firebase
      .firestore()
      .collection("articles")
      .where("id", "==", parseInt(article.id))
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          firebase
            .firestore()
            .collection("articles")
            .doc(doc.id)
            .update({
              title: article.title,
              text: article.text,
              images: article.images,
              author: article.author,
              date: article.date,
            })
            .then(function () {
              history.push("/home");
            })
            .catch(function (error) {
              console.error("Error writing document: ", error);
            });
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  //Delete an article
  async function deleteArticle(articleId) {
    firebase
      .firestore()
      .collection("articles")
      .where("id", "==", parseInt(articleId))
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (docRef) {
          firebase
            .firestore()
            .collection("articles")
            .doc(docRef.id)
            .get()
            .then(function (doc) {
              let images = doc.data().images;

              firebase
                .firestore()
                .collection("articles")
                .doc(doc.id)
                .delete()
                .then(function () {
                  console.log("Document successfully deleted!");

                  if (images.length > 0) {
                    let promise = new Promise((resolve, reject) => {
                      images.forEach((image, index) => {
                        var imageRef = firebase
                          .storage()
                          .ref("images/" + image.name);

                        if (imageRef) {
                          imageRef
                            .delete()
                            .then(function () {
                              console.log("File deleted !");
                            })
                            .catch(function (error) {
                              console.log("Error at deleted file");
                            });
                        }
                        if (index === images.length - 1) resolve();
                      });
                    });

                    promise.then(() => {
                      history.push("/home");
                    });
                  } else {
                    history.push("/home");
                  }
                })
                .catch(function (error) {
                  console.error("Error removing document: ", error);
                });
            });
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  async function uploadImage(file) {
    var options = {
      maxSizeMB: 1,
      useWebWorker: true,
    };
    try {
      const compressedFile = await imageCompression(file, options);
      let storageRef = firebase.storage().ref("images/" + compressedFile.name);

      if (storageRef) {
        return new Promise((resolve, reject) => {
          storageRef.put(compressedFile).then(function (snapshot) {
            storageRef.getDownloadURL().then(function (downloadURL) {
              resolve(downloadURL);
            });
          });
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  async function handlePublishButton() {
    setOnLoading(true);

    let idToRegister;
    let newFiles = [];
    let i = 0;

    if (isArticleExists) {
      idToRegister = currentArticle.id;
    } else {
      idToRegister = lastArticle ? parseInt(lastArticle.id) + 1 : 1;
    }

    let promise = new Promise((resolve, reject) => {
      let nbImages = currentArticle.images.length;
      let toAdd = 100 / (nbImages > 0 ? nbImages : 1);

      currentArticle.images.forEach((file) => {
        uploadImage(file)
          .then((firebaseImageUrl) => {
            let prog = progress;

            setProgress(prog + toAdd);

            let newFile = {
              name: currentArticle.images[i].name,
              size: currentArticle.images[i].size,
              type: currentArticle.images[i].type,
              lastModified: currentArticle.images[i].lastModified,
              lastModifiedDate: currentArticle.images[i].lastModifiedDate,
              storageUrl: firebaseImageUrl,
            };

            newFiles.push(newFile);
          })
          .then(() => {
            i++;

            if (currentArticle.images.length === i) {
              resolve(newFiles);
            }
          });
      });
    });

    let filesWithUrlStorage = await promise;

    let article = {
      id: idToRegister,
      title: currentArticle.title,
      bookId: currentArticle.bookId,
      text: currentArticle.text,
      images: filesWithUrlStorage,
      author: currentArticle.author,
      date: currentArticle.date,
    };

    if (isArticleExists) {
      updateArticle(article);
    } else {
      addArticle(article);
    }
  }

  let deleteButton = "";

  if (isArticleExists && isAdmin) {
    deleteButton = (
      <Button
        style={styles.deleteButton}
        onClick={() => setIsDialogOpen(true)}
        variant="contained"
        color="secondary"
      >
        Supprimer
      </Button>
    );
  }

  if (onLoading) {
    return (
      <div>
        <Menu />
        <div style={styles.loaderText}>
          <p style={styles.uploadText}>
            Upload des photos en cours, ne quittez pas la page
          </p>
        </div>
        <div style={styles.loader}>
          <LinearProgress
            variant="determinate"
            value={progress}
            color="secondary"
          />
        </div>
      </div>
    );
  }

  return (
    <div>
      <Menu />
      <div style={styles.formRoot}>
        <h1 style={styles.title}>{pageTitle}</h1>
        <form noValidate autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
                <DatePicker
                  clearable
                  format="d MMM yyyy"
                  clearLabel="Vider"
                  cancelLabel="Annuler"
                  label="Date de publication"
                  name="targetDate"
                  onChange={(value) =>
                    setCurrentArticle({ ...currentArticle, date: value })
                  }
                  value={currentArticle.date}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12}>
              <FormControl variant="outlined" style={styles.formControl}>
                <InputLabel ref={inputLabel} id="book-input">
                  Carnet
                </InputLabel>
                <Select
                  labelId="book-select"
                  id="book-select"
                  value={currentArticle.book}
                  onChange={(e) =>
                    setCurrentArticle({
                      ...currentArticle,
                      bookId: e.target.value,
                    })
                  }
                  labelWidth={labelWidth}
                >
                  {books.map((book) => {
                    return (
                      <MenuItem key={book.id} value={book.id}>
                        {book.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                style={styles.formElements}
                id="title"
                label="Titre"
                variant="outlined"
                onChange={(e) =>
                  setCurrentArticle({
                    ...currentArticle,
                    title: e.target.value,
                  })
                }
                value={currentArticle.title}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                style={styles.formElements}
                multiline
                id="outlined-basic"
                label="Texte"
                variant="outlined"
                onChange={(e) =>
                  setCurrentArticle({ ...currentArticle, text: e.target.value })
                }
                value={currentArticle.text}
              />
            </Grid>
            <Grid item xs={12}>
              <DropzoneArea
                filesLimit={100}
                dropzoneText="Ajouter des photos"
                acceptedFiles={["image/jpeg", "image/png"]}
                showAlerts={false}
                onChange={(files) =>
                  setCurrentArticle({ ...currentArticle, images: files })
                }
                maxFileSize={20000000}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl variant="outlined" style={styles.formControl}>
                <InputLabel ref={inputLabel} id="author-input">
                  Auteur
                </InputLabel>
                <Select
                  labelId="author-select"
                  id="author-select"
                  value={currentArticle.author}
                  onChange={(e) =>
                    setCurrentArticle({
                      ...currentArticle,
                      author: e.target.value,
                    })
                  }
                  labelWidth={labelWidth}
                >
                  {authors.map((author) => {
                    return (
                      <MenuItem key={author} value={author}>
                        {author}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid style={styles.buttonGrid} item xs={6}>
              <Button
                onClick={() => history.push("/home")}
                variant="contained"
                color="default"
              >
                Annuler
              </Button>
            </Grid>
            <Grid style={styles.buttonGrid} item xs={6}>
              <Button
                onClick={handlePublishButton}
                variant="contained"
                color="primary"
              >
                Publier
              </Button>
            </Grid>
            <Grid style={styles.buttonGrid} item xs={12}>
              {deleteButton}
              <Dialog
                open={isDialogOpen}
                onClose={() => setIsDialogOpen(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Es-tu sûr(e) de vouloir supprimer cet article ?
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={() => setIsDialogOpen(false)}
                    color="primary"
                  >
                    Non
                  </Button>
                  <Button
                    onClick={() => deleteArticle(currentArticle.id)}
                    color="primary"
                    autoFocus
                  >
                    Oui
                  </Button>
                </DialogActions>
              </Dialog>
            </Grid>
          </Grid>
        </form>
      </div>
    </div>
  );
}
