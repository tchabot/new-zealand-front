import React, { useState, useCallback, useEffect } from "react";
import ImagesGallery from "react-photo-gallery";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlane } from "@fortawesome/free-solid-svg-icons";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import moment from "moment";
import Carousel, { Modal, ModalGateway } from "react-images";
import "../../App.css";
import CommentList from "../comment/CommentList";
import { useHistory } from "react-router-dom";
import firebase from "../../firebase";

export default function Article(props) {
  let history = useHistory();
  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);
  const [photos, setPhotos] = useState([]);
  const [photosToDisplay, setPhotosToDisplay] = useState([]);
  const [showMorePhotos, setShowMorePhotos] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    let photos = [];

    props.article.images.forEach((image) => {
      let photo = {};
      photo.src = image.storageUrl;
      photo.height = 2;
      photo.width = 3;
      photos.push(photo);
    });

    if (photos.length > 10) {
      setShowMorePhotos(true);
    }

    setPhotos(photos);
    setPhotosToDisplay(photos.slice(0, 10));
  }, []);

  //Get the user and check if is admin
  useEffect(() => {
    firebase
      .firestore()
      .collection("users")
      .where("uid", "==", firebase.auth().currentUser.uid)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          setIsAdmin(doc.data().role === "admin");
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }, []);

  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };

  const styles = {
    author: {
      marginTop: "30px",
      marginBottom: "30px",
    },
    articleRoot: {
      marginBottom: "30px",
    },
    articleTitle: {
      fontFamily: "'Titillium Web', sans-serif",
      color: "#A66466"
    },
    articleText: {
      textAlign: "justify",
      fontFamily: "roboto",
      fontSize: "17px",
    },
    seeMoreButton: {
      width: "100%",
      height: "79px",
      border: "dashed 1px #8c8787",
      color: "black",
      fontFamily: "monospace",
      fontSize: "16px",
    },
  };

  function loadAllImages() {
    setPhotosToDisplay(photos);
    setShowMorePhotos(false);
  }

  return (
    <div style={styles.articleRoot}>
      <div>
        Date : {moment(props.article.date.toDate()).format("DD/MM/YYYY")}
      </div>
      <Grid item>
        <div>
          <h2 style={styles.articleTitle}>{props.article.title}</h2>
        </div>
      </Grid>
      <Grid item>
        <div>
          {isAdmin && (
            <Button
              onClick={() =>
                history.push("/admin/articles/" + props.article.id)
              }
              variant="contained"
            >
              Modifier
            </Button>
          )}
        </div>
      </Grid>
      <div style={styles.articleText}>
        <p>{props.article.text}</p>
      </div>
      <div>
        <ImagesGallery photos={photosToDisplay} onClick={openLightbox} />
        {showMorePhotos && (
          <Button
            style={styles.seeMoreButton}
            variant="outlined"
            color="primary"
            onClick={() => loadAllImages()}
          >
            Voir plus...
          </Button>
        )}
        <ModalGateway>
          {viewerIsOpen ? (
            <Modal onClose={closeLightbox}>
              <Carousel
                currentIndex={currentImage}
                views={photos.map((x) => ({
                  ...x,
                  srcset: x.srcSet,
                  caption: x.title,
                }))}
              />
            </Modal>
          ) : null}
        </ModalGateway>
      </div>
      <div style={styles.author}>
        Article rédigé par : <b>{props.article.author}</b>
      </div>
      <CommentList article={props.article} />
      <Grid container spacing={3}>
        <Grid item xs={11}>
          <hr />
        </Grid>
        <Grid item xs={1}>
          <FontAwesomeIcon icon={faPlane} />
        </Grid>
      </Grid>
    </div>
  );
}
