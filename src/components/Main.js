import React, { useEffect, useState } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import firebase from "../firebase";
import Menu from "./menu/Menu";
import Article from "./article/Article";
import Footer from "../components/Footer";
import "../App.css";
import { useParams } from "react-router-dom";

export default function Main() {
  const [articles, setArticles] = useState([]);

  let slug = useParams();

  useEffect(() => {
    //Get all articles
    firebase
      .firestore()
      .collection("articles")
      .where("bookId", "==", parseInt(slug.id))
      .orderBy("date", "desc")
      .get()
      .then((querySnapshot) => {
        let retrievedArticles = [];
        querySnapshot.forEach((doc) => {
          retrievedArticles.push(doc.data());
        });
        setArticles(retrievedArticles);
      });
  }, []);

  const styles = {
    container: {
      marginTop: "20px",
      position: "relative",
      paddingBottom: "60px",
    },
  };

  return (
    <div id="mainDiv" style={styles.container}>
      <CssBaseline />
      <Menu />
      <Container>
        {articles.map((article, index) => {
          return <Article key={index} article={article} />;
        })}
      </Container>
      <Footer />
    </div>
  );
}
