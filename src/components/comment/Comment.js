import React from "react";
import moment from "moment";

export default function Comment(props) {
    const styles = {
        authorName: {
            fontFamily: "Open Sans, sans-serif",
            fontSize: "1rem",
            fontWeight: "normal",
            lineHeight: "1.5",
            color: "#595959"
        },
        date: {
            fontFamily: "Open Sans, sans-serif",
            fontSize: "1rem",
            fontWeight: "lighter",
            lineHeight: "1.5",
            color: "#595959",
        },
        text: {
            fontFamily: "Open Sans, sans-serif",
            fontSize: "16px",
            fontWeight: "normal",
            lineHeight: "1.5",
            color: "#595959"
        },
        rootDiv: {
            marginBottom: "30px"
        }
    };

    return (
        <div style={styles.rootDiv}>
            <div>
                <span style={styles.authorName}>{props.comment.authorName ? props.comment.authorName : 'Anonyme'}</span>
            </div>
            <br/>
            <div>
                <span style={styles.date}>Le {props.comment.date ? moment.unix(props.comment.date.seconds).format('DD/MM/YYYY') : ''}</span>
            </div>
            <div>
                <p style={styles.text}>{props.comment.text ? props.comment.text : ''}</p>
            </div>
            <hr/>
        </div>
    );
}
