import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Comment from "./Comment";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import firebase from "../../firebase";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  listRoot: {
    backgroundColor: "#f5f5f5",
  },
  commentInput: {
    width: "100%",
  },
  buttonGrid: {
    display: "flex",
    justifyContent: "center",
  },
  circleNumber: {
    borderRadius: "50%",
    width: "30px",
    height: "30px",
    padding: "5px",
    background: "#888",
    border: "1px solid #666",
    color: "#FFF",
    textAlign: "center",
    font: "19px Arial, sans-serif",
  },
  listTitle: {
    marginLeft: "10px",
  },
  commentsZone: {
    backgroundColor: "#f5f5f5",
    padding: "30px",
  },
  postComment: {
    marginTop: "20px",
  },
  mainTitleComment: {
    fontSize: "1.5rem",
    fontFamily: "Open Sans, sans-serif",
    color: "#595959",
  },
  registerToComment: {
    fontFamily: "Open Sans, sans-serif",
    color: "#595959",
  },
}));

export default function CommentList(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [text, setText] = React.useState("");
  const [notificationRegister, setNotificationRegister] = React.useState(false);
  const [authorName, setAuthorName] = React.useState("");
  const [authorEmail, setAuthorEmail] = React.useState("");

  function handleSubmit() {
    let comment = {
      text: text,
      authorName: authorName,
      authorEmail: authorEmail,
      date: new Date(),
    };

    firebase
      .firestore()
      .collection("articles")
      .where("id", "==", parseInt(props.article.id))
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          firebase
            .firestore()
            .collection("articles")
            .doc(doc.id)
            .update({
              comments: firebase.firestore.FieldValue.arrayUnion(comment),
            })
            .catch(function (error) {
              console.error("Error writing document: ", error);
            });
        });
      })
      .then(() => window.location.reload(true))
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  return (
    <List component="nav" className={classes.root}>
      <ListItem
        className={classes.listRoot}
        button
        onClick={() => setOpen(!open)}
      >
        <div className={classes.circleNumber}>
          {props.article.comments ? props.article.comments.length : 0}
        </div>
        <ListItemText className={classes.listTitle} primary="Commentaires" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse
        className={classes.commentsZone}
        in={open}
        timeout="auto"
        unmountOnExit
      >
        {props.article.comments ? (
          props.article.comments.map((comment, index) => {
            return <Comment key={index} comment={comment} />;
          })
        ) : (
          <div></div>
        )}
        <h3 className={classes.mainTitleComment}>Poster un commentaire</h3>
        <div className={classes.postComment}>
          <form noValidate autoComplete="off">
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  id="comment-text"
                  className={classes.commentInput}
                  multiline
                  rows="4"
                  variant="outlined"
                  value={text}
                  onChange={(e) => setText(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={notificationRegister}
                      onChange={(e) =>
                        setNotificationRegister(e.target.checked)
                      }
                      value="notificationRegister"
                    />
                  }
                  label=" Souhaitez-vous être informé par e-mail des réponses à ce message ?"
                  className={classes.registerToComment}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className={classes.commentInput}
                  id="authorName"
                  label="Votre nom"
                  variant="outlined"
                  value={authorName}
                  onChange={(e) => setAuthorName(e.target.value)}
                />
              </Grid>
              <Grid item xs={12} className={classes.commentInput}>
                <TextField
                  className={classes.commentInput}
                  id="authorEmail"
                  label="Votre email"
                  variant="outlined"
                  value={authorEmail}
                  onChange={(e) => setAuthorEmail(e.target.value)}
                />
              </Grid>
              <Grid item xs={6} className={classes.buttonGrid}>
                <Button
                  onClick={() => setOpen(false)}
                  variant="contained"
                  color="default"
                >
                  Annuler
                </Button>
              </Grid>
              <Grid item xs={6} className={classes.buttonGrid}>
                <Button
                  onClick={() => handleSubmit()}
                  variant="contained"
                  color="primary"
                >
                  Publier
                </Button>
              </Grid>
            </Grid>
          </form>
        </div>
      </Collapse>
    </List>
  );
}
