import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router-dom";

const loginImageKitty = require("../../images/new-zealand-landscape.jpg");

const useStyles = makeStyles({
  root: {
    maxWidth: 400,
    margin: "0px 15px 15px 15px",
    width: "100%",
    cursor: "pointer",
  },
  media: {
    height: 140,
  },
});

export default function Book(props) {
  let history = useHistory();
  const classes = useStyles();

  return (
    <Card className={classes.root} onClick={() => history.push("/books/"+props.book.id+"/articles")}>
      <CardActionArea>
        <CardMedia className={classes.media} image={loginImageKitty} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.book.name}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button
          size="small"
          color="primary"
          onClick={() => history.push("/books/"+props.book.id+"/articles")}
        >
          Découvrir
        </Button>
      </CardActions>
    </Card>
  );
}
