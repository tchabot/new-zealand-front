import React, { useEffect, useState } from "react";
import Menu from "../menu/Menu";
import Book from "./Book";
import firebase from "../../firebase";
import { Container } from "@material-ui/core";
import Footer from "../Footer";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";

const styles = {
  cardList: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  addButton: {
    position: "fixed",
    bottom: "70px",
    right: "20px",
    backgroundColor: "#A66466",
  },
  addIcon: {
    color: "#fff"
  },
  mainDiv: {
    position: "relative",
    paddingBottom: "55px",
    minHeight: "100vh"
  }
};

export default function BookList() {
  const [books, setBooks] = React.useState([]);
  const [isAdmin, setIsAdmin] = useState(false);
  const [lastBook, setLastBook] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [bookName, setBookName] = React.useState(null);

  useEffect(() => {
    //Retrieve all books
    firebase
      .firestore()
      .collection("books")
      .orderBy("createdAt", "desc")
      .get()
      .then((querySnapshot) => {
        let retrievedBooks = [];
        querySnapshot.forEach((doc) => {
          retrievedBooks.push(doc.data());
        });
        setBooks(retrievedBooks);
      });

    //Get and set user role
    firebase
      .firestore()
      .collection("users")
      .where("uid", "==", firebase.auth().currentUser.uid)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          setIsAdmin(doc.data().role === "admin");
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });

    //Retrieve the last book
    firebase
      .firestore()
      .collection("books")
      .orderBy("createdAt", "desc")
      .limit(1)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          setLastBook(doc.data());
        });
      });
  }, []);

  function handleCreateBook() {
    setOpen(false);

    let book = {
      id: lastBook ? lastBook.id + 1 : 1,
      name: bookName,
    };

    firebase
      .firestore()
      .collection("books")
      .add({
        id: book.id,
        name: book.name,
        createdAt: new Date(),
        updatedAt: null,
      })
      .then(function () {
        window.location.reload(true);
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
  }

  return (
    <div style={styles.mainDiv}>
      <Menu />
      <Container style={styles.Container}>
        <div style={styles.cardList}>
          {books.map((book) => {
            return <Book key={book.id} book={book} />;
          })}
          {isAdmin && (
            <Fab
              onClick={() => setOpen(true)}
              aria-label="add"
              size="medium"
              style={styles.addButton}
            >
              <AddIcon style={styles.addIcon}/>
            </Fab>
          )}
        </div>
      </Container>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Création d'un nouveau carnet
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="bookName"
            label="Nom du carnet"
            type="text"
            onChange={(e) => setBookName(e.target.value)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} color="primary">
            Annuler
          </Button>
          <Button onClick={() => handleCreateBook()} color="primary">
            Créer
          </Button>
        </DialogActions>
      </Dialog>
      <Footer />
    </div>
  );
}
