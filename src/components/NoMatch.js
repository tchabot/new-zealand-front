import React from "react";
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";

export default function () {
  const history = useHistory();
  const styles = {
    img: {
      maxWidth: "80%",
    },
  };

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      spacing={3}
    >
      <Grid container justify="center" item xs={12}>
        <img
          style={styles.img}
          src={require("../images/sad-kitten-2.png")}
          alt={"404"}
        />
      </Grid>
      <Grid container justify="center" item xs={12}>
        <div>Oops ! La page demandée n'existe pas</div>
      </Grid>
      <Grid container justify="center" item xs={12}>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => history.push("/books")}
        >
          Revenir à l'accueil
        </Button>
      </Grid>
    </Grid>
  );
}
