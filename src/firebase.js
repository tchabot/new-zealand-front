import * as firebase from "firebase";

const config = {
  apiKey: "AIzaSyCm05BKi8hdCvsLh_bJWo0iq7Grt6FxHgw",
  authDomain: "new-zealand-7543e.firebaseapp.com",
  databaseURL: "https://new-zealand-7543e.firebaseio.com",
  projectId: "new-zealand-7543e",
  storageBucket: "new-zealand-7543e.appspot.com",
  messagingSenderId: "811802032204",
  appId: "1:811802032204:web:7ecfe0758fbc9582901ce9",
  measurementId: "G-52JWHB0MF6",
};

firebase.initializeApp(config);

export default firebase;
