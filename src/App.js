import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Register from "./components/account/Register";
import Main from "./components/Main";
import Login from "./components/account/Login";
import ArticleDetail from "./components/article/ArticleDetail";
import BookList from "./components/book/BookList";
import firebase from "./firebase";
import NoMatch from "./components/NoMatch";
import LegalNotice from "./components/LegalNotice";
import PrivateRoute from "./components/PrivateRoute";

export default class App extends React.Component {
  state = {
    authenticated: false,
    loading: true,
  };

  componentWillMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          authenticated: true,
          loading: false,
        });
      } else {
        this.setState({
          authenticated: false,
          loading: false,
        });
      }
    });
  }

  render() {
    const { authenticated, loading } = this.state;

    if (loading) {
      return <p></p>;
    }

    return (
      <Router>
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route exact path="/register">
            <Register />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <PrivateRoute exact path="/books/:id/articles" authenticated={authenticated}>
            <Main />
          </PrivateRoute>
          <PrivateRoute
            exact
            path="/admin/articles/:id"
            authenticated={authenticated}
          >
            <ArticleDetail />
          </PrivateRoute>
          <PrivateRoute
            exact
            path="/admin/articles/new"
            authenticated={authenticated}
          >
            <ArticleDetail />
          </PrivateRoute>
          <PrivateRoute exact path="/books" authenticated={authenticated}>
            <BookList />
          </PrivateRoute>
          <Route exact path="/legal-notice">
            <LegalNotice />
          </Route>
          <Route path="*">
            <NoMatch />
          </Route>
        </Switch>
      </Router>
    );
  }
}
